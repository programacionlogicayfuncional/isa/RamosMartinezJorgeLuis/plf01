(ns plf01.core)

(defn función-<-1
  [a]
  (< a))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))

(función-<-1 7)
(función-<-2 5 9)
(función-<-3 5 10 15)

(defn función-<=-1
  [a]
  (<= a))

(defn función-<=-2
  [a b]
  (<= a b))

(defn función-<=-3
  [a b c]
  (<= a b c))

(función-<=-1 4)
(función-<=-2 8 8)
(función-<=-3 8 10 12)

(defn función-==-1
  [a]
  (== a))

(defn función-==-2
  [a b]
  (== a b))

(defn función-==-3
  [a b c]
  (== a b c))

(función-==-1 1)
(función-==-2 1/1 2/2)
(función-==-3 1 1.0 1/1)

(defn función->-1
  [a b]
  (> a))

(defn función->-2
  [a b c]
  (> a b))

(defn función->-3
  [a b c d]
  (> a b c))

(función->-1 2 1)
(función->-2 6 5 4)
(función->-3 1 2 3 4)

(defn función->=-1
  [a b]
  (>= a b))

(defn función->=-2
  [a b c]
  (>= a b c))

(defn función->=-3
  [a b c d e]
  (>= a b c d e))

(función->=-1 1/2 0.5)
(función->=-2 1/2 1/3 1/4)
(función->=-3 6 5 4 3 2)

(defn función-assoc-1
  [coll k v]
  (assoc coll k v))

(defn función-assoc-2
  [coll k v k2 v2]
  (assoc coll k v k2 v2))

(defn función-assoc-3
  [coll k v k2 v2 k3 v3]
  (assoc coll k v k2 v2 k3 v3))

(función-assoc-1 {} :key1 "value")
(función-assoc-2 nil :uno 1 :dos 2)
(función-assoc-3 [1 4 5 6] 1 2 2 3 3 4)

(defn función-assoc-in-1
  [m ks v]
  (assoc-in m ks v))

(defn función-assoc-in-2
  [m ks v]
  (assoc-in m ks v))

(defn función-assoc-in-3
  [m ks v]
  (assoc-in m ks v))


(def usuarios [{:nombre "Jorge" :edad 20}
               {:nombre "Luis" :edad 50}])
(función-assoc-in-1 usuarios [0 :edad] 21)
(función-assoc-in-2 [{:personas {:nombre ["Jorge"]}}] [0 :personas :nombre 1] "Luis")
(función-assoc-in-3 {:pasatiempos {:deportes "Baloncesto"}} [:pasatiempos :deportes] "Fútbol")


(defn función-concat-1
  [a b]
  (concat a b))

(defn función-concat-2
  [a b c]
  (concat a b c))

(defn función-concat-3
  [a b c d]
  (concat a b c d))

(función-concat-1 [1 2] [3 4])
(función-concat-2 "abc" "def" "ghi")
(función-concat-3 {:a "A" :b "B"} {:c "C" :d "D"} {:e "E" :f "F"} {:g "G" :h "H"})

(defn función-conj-1
  [coll x]
  (conj coll x))

(defn función-conj-2
  [coll x y]
  (conj coll x y))

(defn función-conj-3
  [coll x y z]
  (conj coll x y z))

(función-conj-1 [1 2 3] 4)
(función-conj-2 [[1 2] [3 4]] [4 5] [6 7])
(función-conj-3 #{1 5 6} 2 3 4)
(función-conj-1 {:nombre "Jorge" :apellido "Martínez"} {:edad 21 :nacionalidad "Mexicana"})

(defn función-cons-1
  [x seq]
  (cons x seq))

(defn función-cons-2
  [x y seq]
  (cons x (cons y seq)))

(defn función-cons-3
  [x y z seq]
  (cons x (cons y (cons z seq))))

(función-cons-1 1 '(2 3 4 5 6))
(función-cons-2 3 4 [5 6 7 8])
(función-cons-3 1 2 3 #{4 5 6})

(defn función-contains?-1
  [coll key]
  (contains? coll key))

(defn función-contains?-2
  [coll key]
  (contains? coll key))

(defn función-contains?-3
  [coll key]
  (contains? coll key))

(función-contains?-1 [0 1 2 3] 2)
(función-contains?-2 {:a "A" :b "B" :c "C"} :b)
(función-contains?-3 #{"a" "b" "c"} "z")

(defn función-count-1
  [coll]
  (count coll))

(defn función-count-2
  [coll]
  (count coll))

(defn función-count-3
  [coll]
  (count coll))

(función-count-1 [1 2 3 4 5 6])
(función-count-2 #{:a :b :c :d})
(función-count-3 {:uno 1 :dos 2 :tres 3})

(defn función-disj-1
  [set key]
  (disj set key))

(defn función-disj-2
  [set key]
  (disj set key))

(defn función-disj-3
  [set key key2]
  (disj set key key2))

(función-disj-1 #{1 2 3} 2)
(función-disj-2 #{1 2 3 4 5} [1 2])
(función-disj-3 #{1 2 3 4} 1 3)

(defn función-dissoc-1
  [map]
  (dissoc map))

(defn función-dissoc-2
  [map key]
  (dissoc map key))

(defn función-dissoc-3
  [map key1 key2]
  (dissoc map key1 key2))

(función-dissoc-1 {:a "A" :b "B" :c "C"})
(función-dissoc-2 {:a "A" :b "B" :c "C"} :b)
(función-dissoc-3 {:uno 1 :dos 2 :tres 3} :uno :dos)

(defn función-distinct-1
  [coll]
  (distinct coll))

(defn función-distinct-2
  [coll]
  (distinct coll))

(defn función-distinct-3
  [coll]
  (distinct coll))

(función-distinct-1 [1 2 1 3 1 4 1 5])
(función-distinct-2 [90 80 80 90 70 100])
(función-distinct-3 [\a \b \c \a \b \c])

(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c d]
  (distinct? a b c d))

(función-distinct?-1 1 2)
(función-distinct?-2 1 2 2)
(función-distinct?-3 1 2 3 4)

(defn función-drop-last-1
  [coll]
  (drop-last coll))

(defn función-drop-last-2
  [n coll]
  (drop-last n coll))

(defn función-drop-last-3
  [n coll]
  (drop-last n coll))

(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 2 [1 2 3 4 5 6])
(función-drop-last-3 -1 [1 2 3 4 5])
(función-drop-last-3 5 [1 2 3])

(defn función-empty-1
  [coll]
  (empty coll))

(defn función-empty-2
  [coll]
  (empty coll))

(defn función-empty-3
  [coll]
  (empty coll))

(función-empty-1 '(1 2 3 4))
(función-empty-2 [1 2 3 4 5 6 7])
(función-empty-3 {1 2 3 4})
(función-empty-3 #{1 2 3 5 6})

(defn función-empty?-1
  [coll]
  (empty? coll))

(defn función-empty?-2
  [coll]
  (empty? coll))

(defn función-empty?-3
  [coll]
  (empty? coll))

(función-empty?-1 [])
(función-empty?-2 '())
(función-empty?-3 nil)

(defn función-even?-1
  [número]
  (even? número))

(defn función-even?-2
  [número]
  (even? número))

(defn función-even?-3
  [número]
  (even? número))

(función-even?-1 6)
(función-even?-2 10)
(función-even?-3 3)

(defn función-false?-1
  [a]
  (false? a))

(defn función-false?-2
  [a]
  (false? a))

(defn función-false?-3
  [a]
  (false? a))

(función-false?-1 false)
(función-false?-2 nil)
(función-false?-3 "valor")

(defn función-find-1
  [mapa llave]
  (find mapa llave))

(defn función-find-2
  [mapa llave]
  (find mapa llave))

(defn función-find-3
  [mapa llave]
  (find mapa llave))

(función-find-1 {:uno 1 :dos 2 :tres 3} :dos)
(función-find-2 {:a "A" :b "B" :c "C"} :c)
(función-find-3 {:a \a :b \b :c \c} :a)

(defn función-first-1
  [colección]
  (first colección))

(defn función-first-2
  [colección]
  (first colección))

(defn función-first-3
  [colección]
  (first colección))

(función-first-1 [])
(función-first-2 #{3 4 5})
(función-first-3 [1 2 3 4])

(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a]
  (flatten a))

(función-flatten-1 [1 [2 [3 [4]]]])
(función-flatten-2 '(1 2 [3 (4 5 6)]))
(función-flatten-3 nil)

(defn función-frequencies-1
  [colección]
  (frequencies colección))

(defn función-frequencies-2
  [colección]
  (frequencies colección))

(defn función-frequencies-3
  [colección]
  (frequencies colección))

(función-frequencies-1 [1 1 2 3 5 5 2])
(función-frequencies-2 ["A" "A" "A" "B" "C" "C"])
(función-frequencies-3 [:a :a :b :c :c :c :d])

(defn función-get-1
  [mapa llave]
  (get mapa llave))

(defn función-get-2
  [mapa llave default]
  (get mapa llave default))

(defn función-get-3
  [mapa llave default]
  (get mapa llave default))

(función-get-1 [1 2 3 4] 3)
(función-get-2 {:a "A" :b "B" :c "C"} :z "No encontrado")
(función-get-3 [1 2 3] 5 100)

(defn función-get-in-1
  [m ks]
  (get-in m ks))

(defn función-get-in-2
  [m ks]
  (get-in m ks))

(defn función-get-in-3
  [m ks default]
  (get-in m ks default))

(def empleados {:nombre "Jorge"
                :detalles {:email "jorge@correo.com"
                           :telefono "345-234-32"}})
(función-get-in-1 empleados [:detalles :email])
(función-get-in-2 empleados [:detalles :sexo])
(función-get-in-3 empleados [:detalles :sexo] "no-encontrado")

(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

(función-into-1 [] {1 2, 3 4})
(función-into-2 [1 2 3] '(4 5 6))
(función-into-3 {:a "A"} [{:b "B" :c "C"}])

(defn función-key-1
  [llave mapa]
  (mapa key llave))

(defn función-key-2
  [llave mapa]
  (mapa key llave))

(defn función-key-3
  [llave mapa]
  (mapa key llave))

(función-key-1 :A {:A 1})
(función-key-2 :nombre {:nombre "Jorge" :apellido "Martínez"})
(función-key-3 :edad {:edad 23 :sexo "Masculino"})

(defn función-keys-1
  [m]
  (keys m))

(defn función-keys-2
  [m]
  (keys m))

(defn función-keys-3
  [m]
  (keys m))

(función-keys-1 {:a "A" :b "B" :c "C" :d "D"})
(función-keys-2 {})
(función-keys-3 {:uno 1 :dos 2 :tres 3})

(defn función-max-1
  [a b]
  (max a b))

(defn función-max-2
  [a b c]
  (max a b c))

(defn función-max-3
  [a b c d]
  (max a b c d))

(función-max-1 1 2)
(función-max-2 5 2 7)
(función-max-3 8 0 3 2)

(defn función-merge-1
  [a b]
  (merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b]
  (merge a b))

(función-merge-1 {:uno 1 :dos 2} {:tres 3 :cuatro 4})
(función-merge-2 {:a 1} nil)
(función-merge-3 nil nil)

(defn función-min-1
  [a b]
  (min a b))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a b c d]
  (min a b c d))

(función-min-1 5 6)
(función-min-2 9 3 6)
(función-min-3 0 3 4 1)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a]
  (neg? a))

(defn función-neg?-3
  [a]
  (neg? a))

(función-neg?-1 -4)
(función-neg?-2 2)
(función-neg?-3 1/4)

(defn función-nil?-1
  [a]
  (nil? a))

(función-neg?-1 -4)
(función-neg?-2 2)
(función-neg?-3 1/4)

(defn función-nil?-2
  [a]
  (nil? a))

(función-neg?-1 -4)
(función-neg?-2 2)
(función-neg?-3 1/4)

(defn función-nil?-3
  [a]
  (nil? a))

(función-nil?-1 nil)
(función-nil?-2 2)
(función-nil?-3 true)

(defn función-not-empty-1
  [colección]
  (not-empty colección))

(defn función-not-empty-2
  [colección]
  (not-empty colección))

(defn función-not-empty-3
  [colección]
  (not-empty colección))

(función-not-empty-1 [])
(función-not-empty-2 {})
(función-not-empty-3 '(2 3 5))

(defn función-nth-1
  [colección indice]
  (nth colección indice))

(defn función-nth-2
  [colección indice]
  (nth colección indice))

(defn función-nth-3
  [colección indice default]
  (nth colección indice default))

(función-nth-1 [1 2 3 4] 2)
(función-nth-2 ["a" "b" "c"] 0)
(función-nth-3 [] 2 "no-encontrado")

(defn función-odd?-1
  [a]
  (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

(función-odd?-1 4)
(función-odd?-2 5)
(función-odd?-3 11)

(defn función-partition-1
  [n colección]
  (partition n colección))

(defn función-partition-2
  [n paso colección]
  (partition n paso colección))

(defn función-partition-3
  [n paso reutilizar colección]
  (partition n paso reutilizar colección))

(función-partition-1 2 [1 2 3 4 5 6 7 8 9 10])
(función-partition-2 2 3 [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15])
(función-partition-3 2 3 ["a"] [1 2 3 4 5 6 7 8 9 10])

(defn función-partition-all-1
  [n colección]
  (partition-all n colección))

(defn función-partition-all-2
  [n colección]
  (partition-all n colección))

(defn función-partition-all-3
  [n paso colección]
  (partition-all n paso colección))

(función-partition-all-1 4 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-2 2 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-3 2 4 [0 1 2 3 4 5 6 7 8 9])

(defn función-peek-1
  [colección]
  (peek colección))

(defn función-peek-2
  [colección]
  (peek colección))

(defn función-peek-3
  [colección]
  (peek colección))

(función-peek-1 [])
(función-peek-2 [1 2 3 4 5 6])
(función-peek-3 '(5 2 3 4))

(defn función-pop-1
  [colección]
  (pop colección))

(defn función-pop-2
  [colección]
  (pop colección))

(defn función-pop-3
  [colección]
  (pop colección))

(función-pop-1 [1 2 3 4 5])
(función-pop-2 '(4 6 7 3 5))
(función-pop-3 [1 2 3 4])

(defn función-pos?-1
  [número]
  (pos? número))

(defn función-pos?-2
  [número]
  (pos? número))

(defn función-pos?-3
  [número]
  (pos? número))

(función-pos?-1 5)
(función-pos?-2 -2/3)
(función-pos?-3 3.4)

(defn función-quot-1
  [numerador denominador]
  (quot numerador denominador))

(defn función-quot-2
  [numerador denominador]
  (quot numerador denominador))

(defn función-quot-3
  [numerador denominador]
  (quot numerador denominador))

(función-quot-1 10 3)
(función-quot-2 12 6)
(función-quot-3 15 3)

(defn función-range-1
  [final]
  (range final))

(defn función-range-2
  [inicio final]
  (range inicio final))

(defn función-range-3
  [inicio final paso]
  (range inicio final paso))

(función-range-1 10)
(función-range-2 5 20)
(función-range-3 1 20 2)

(defn función-rem-1
  [numerador denominador]
  (rem numerador denominador))

(defn función-rem-2
  [numerador denominador]
  (rem numerador denominador))

(defn función-rem-3
  [numerador denominador]
  (rem numerador denominador))

(función-rem-1 10 2)
(función-rem-2 100 3)
(función-rem-3 24 7)

(defn función-repeat-1
  [n x]
  (repeat n x))

(defn función-repeat-2
  [n x]
  (repeat n x))

(defn función-repeat-3
  [n x]
  (repeat n x))

(función-repeat-1 3 "Jorge")
(función-repeat-2 10 2)
(función-repeat-3 3 "x")

(defn función-replace-1
  [x y]
  (replace x y))

(defn función-replace-2
  [x y]
  (replace x y))

(defn función-replace-3
  [x y]
  (replace x y))

(función-replace-1 [10 9 8 7 6] [0 2 4])
(función-replace-2 {1 :uno 2 :dos 3 :tres} [1 2 3 4])
(función-replace-3 '{0 cero, 1 uno, 2 dos} '(0 1 2))

(defn función-rest-1
  [colección]
  (rest colección))

(defn función-rest-2
  [colección]
  (rest colección))

(defn función-rest-3
  [colección]
  (rest colección))

(función-rest-1 [1 2 3 4 5])
(función-rest-2 ["a" "b" "c" "d"])
(función-rest-3 '())

(defn función-select-keys-1
  [mapa seq-keys]
  (select-keys mapa seq-keys))

(defn función-select-keys-2
  [mapa seq-keys]
  (select-keys mapa seq-keys))

(defn función-select-keys-3
  [mapa seq-keys]
  (select-keys mapa seq-keys))

(función-select-keys-1 {:a "A" :b "B" :c "C"} [:b :c])
(función-select-keys-2 {:uno 1 :dos 2 :tres 3} [:uno])
(función-select-keys-3 {:a \a :e \e :i \i :o \o :u \u} [:a :u])

(defn función-shuffle-1
  [colección]
  (shuffle colección))

(defn función-shuffle-2
  [colección]
  (shuffle colección))

(defn función-shuffle-3
  [colección]
  (shuffle colección))

(función-shuffle-1 [1 2 3])
(función-shuffle-2 [8 9 3 4 6 7])
(función-shuffle-3 [0 8 2 3])

(defn función-sort-1
  [colección]
  (sort colección))

(defn función-sort-2
  [colección]
  (sort colección))

(defn función-sort-3
  [colección]
  (sort colección))

(función-sort-1 [4 3 2])
(función-sort-2 [9 3 4 5 6])
(función-sort-3 [9 2 3 4 5 6 5])

(defn función-split-at-1
  [número colección]
  (split-at número colección))

(defn función-split-at-2
  [número colección]
  (split-at número colección))

(defn función-split-at-3
  [número colección]
  (split-at número colección))

(función-split-at-1 2 [1 2 3 4 5])
(función-split-at-2 5 [6 8 2 3 5 3 2 4])
(función-split-at-3 2 [4 6 2])

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a b]
  (str a b))

(defn función-str-3
  [a b c]
  (str a b c))

(función-str-1 2)
(función-str-2 "hola" " mundo")
(función-str-3 "2" "0" 20)

(defn función-subs-1
  [cadena inicio]
  (subs cadena inicio))

(defn función-subs-2
  [cadena inicio]
  (subs cadena inicio))

(defn función-subs-3
  [cadena inicio final]
  (subs cadena inicio final))

(función-subs-1 "Hola mundo" 3)
(función-subs-2 "programación" 5)
(función-subs-3 "programación" 2 4)

(defn función-subvec-1
  [vector inicio]
  (subvec vector inicio))

(defn función-subvec-2
  [vector inicio]
  (subvec vector inicio))

(defn función-subvec-3
  [vector inicio final]
  (subvec vector inicio final))

(función-subvec-1 [1 2 3 4 5] 2)
(función-subvec-2 [4 5 6 2 4 2 3 4] 5)
(función-subvec-3 [2 5 4 2 2 3] 2 4)

(defn función-take-1
  [número colección]
  (take número colección))

(defn función-take-2
  [número colección]
  (take número colección))

(defn función-take-3
  [número colección]
  (take número colección))

(función-take-1 3 [1 2 3 4 5 6 7])
(función-take-2 3 '(1 2 3 4 5))
(función-take-3 1 [])

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 (== 1 1))
(función-true?-2 true)
(función-true?-3 1.2)

(defn función-val-1
  [valor mapa]
  (mapa val valor))

(defn función-val-2
  [valor mapa]
  (mapa val valor))

(defn función-val-3
  [valor mapa]
  (mapa val valor))

(función-val-1 "A" {:a "A"})
(función-val-2 "1234" {:usuario "admin" :password "1234"})
(función-val-3 21 {:edad 21 :sexo "Masculino"})

(defn función-vals-1
  [mapa]
  (vals mapa))

(defn función-vals-2
  [mapa]
  (vals mapa))

(defn función-vals-3
  [mapa]
  (vals mapa))

(función-vals-1 {:uno 1 :dos 2 :tres 3})
(función-vals-2 {:a "A" :b "B" :c "C"})
(función-vals-3 {:a \a :b \b :c \c})

(defn función-zero?-1
  [número]
  (zero? número))

(defn función-zero?-2
  [número]
  (zero? número))

(defn función-zero?-3
  [número]
  (zero? número))

(función-zero?-1 0.0)
(función-zero?-2 1/6)
(función-zero?-3 0x0)

(defn función-zipmap-1
  [llave valor]
  (zipmap llave valor))

(defn función-zipmap-2
  [llave valor]
  (zipmap llave valor))

(defn función-zipmap-3
  [llave valor]
  (zipmap llave valor))

(función-zipmap-1 [:uno :dos :tres] [1 2 3])
(función-zipmap-2 [:a :b :c :d] [\a \b \c \d])
(función-zipmap-3 [:nombre :apellido] ["Jorge" "Martínez"])